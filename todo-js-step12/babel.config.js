/* eslint-env node*/
module.exports = function(api) {
  const isTest = api.env('test');

  const presets = [
    [
      '@babel/env',
      {
        targets: {
          ie: '11',
          edge: '83',
          firefox: '68',
          chrome: '84',
          safari: '13'
        },
        useBuiltIns: 'entry',
        corejs: 3,
        modules: isTest ? 'commonjs' : false
      }
    ]
  ];
  const plugins = [];

  api.cache(true);

  return { presets, plugins };
};
