context('Manage ToDo items', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4567/');
  });

  it('Add a todo item then remove it again', () => {
    cy.get('#todo-text')
      .type('Learn Cypress').should('have.value', 'Learn Cypress');

    cy.get('#add-button').click();

    let itemCountAfterAdd;
    cy.get('#todo-list li')
      .then(($items) => {
        itemCountAfterAdd = $items.length;
      })
      .last()
      .should('contain', 'Learn Cypress');

    cy.get('#todo-list li button')
      .last()
      .click();

    cy.get('#todo-list')
      .should('not.contain', 'Learn Cypress');

    cy.get('#todo-list li')
      .then(($items) => {
        expect($items.length).to.eq(itemCountAfterAdd - 1);
      });
  });
});
