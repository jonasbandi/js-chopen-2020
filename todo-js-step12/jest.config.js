/* eslint-env node */

module.exports = {
  roots: ['src'],
  reporters: ['default', ['jest-junit', { outputDirectory: './build-info' }]]
};
