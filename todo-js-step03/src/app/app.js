var app = document.getElementById('app');

function renderForm() {
  var form = document.createElement('form');
  form.classList.add('new-todo');

  var todoInput = document.createElement('input');
  todoInput.id = 'todo-text';
  todoInput.placeholder = 'What do you want to do?';
  var addButton = document.createElement('button');
  addButton.classList.add('add-button');
  addButton.innerText = '+';
  addButton.addEventListener('click', function(event) {
    event.preventDefault();
    addToDo(todoInput);
  });

  form.appendChild(todoInput);
  form.appendChild(addButton);

  app.appendChild(form);
}

function renderToDoList() {
  var listContainer = document.createElement('div');
  listContainer.classList.add('todo-list-container');
  var todoList = document.createElement('ul');
  todoList.id = 'todo-list';
  todoList.classList.add('todo-list');

  listContainer.appendChild(todoList);

  app.appendChild(listContainer);
}

function renderApp() {
  app.innerHTML = '';
  renderForm();
  renderToDoList();
}
renderApp();

function addToDo(input) {
  var textValue = input.value;

  if (textValue) {
    var listItem = document.createElement('li');
    listItem.innerHTML = textValue;

    var button = document.createElement('button');
    button.innerHTML = 'X';
    button.addEventListener('click', removeToDo);
    listItem.appendChild(button);

    document.getElementById('todo-list').appendChild(listItem);
    input.value = '';
  }
}

function removeToDo() {
  var item = this.closest('li');
  item.remove();
  // var item = this.parentElement;
  // document.getElementById("todo-list").removeChild(item);
}
