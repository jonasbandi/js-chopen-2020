import '../css/index.css';
import $ from 'jquery';
import { applyLayout } from './layout.js';

console.log('Starting app ...');

applyLayout();

var $app = $('#app');

function renderForm() {
  var $input = $('<input>', {
    id: 'todo-text',
    placeholder: 'What do you want to do?'
  });
  $('<form>')
    .addClass('new-todo')
    .append(
      $input,
      $('<button>')
        .addClass('add-button')
        .text('+')
        .on('click', function(event) {
          event.preventDefault();
          addToDo($input);
        })
    )
    .appendTo($app);
}

function renderToDoList() {
  $('<div>')
    .addClass('todo-list-container')
    .append(
      $('<ul>')
        .prop('id', 'todo-list')
        .addClass('todo-list')
    )
    .appendTo($app);
}

function renderApp() {
  $app.html('');
  renderForm();
  renderToDoList();
}

renderApp();

function addToDo($input) {
  var textValue = $input.val();
  if (textValue) {
    $('<li>')
      .appendTo('#todo-list')
      .text(textValue)
      .append(
        $('<button>')
          .text('X')
          .on('click', removeToDo)
      );
    $input.val('');
  }
}

function removeToDo() {
  var item = $(this).closest('li');
  item.remove();
}
