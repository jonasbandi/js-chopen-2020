module.exports = function(api) {
  const presets = [
    [
      '@babel/env',
      {
        targets: {
          ie: '11',
          edge: '83',
          firefox: '68',
          chrome: '84',
          safari: '13'
        },
        useBuiltIns: 'entry',
        modules: false
      }
    ]
  ];
  const plugins = [];

  api.cache(true);

  return { presets, plugins };
};
