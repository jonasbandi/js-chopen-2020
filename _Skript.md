## Step 1: Implementing the First Feature

Start with the project in the directory `todo-js-start` , open `index.html`.
Implement the functionality to add todo items using plain DOM APIs.

The following snippet contains the a basic implementation of the needed DOM manipulation:

```js
function addToDo(){
	var input = document.getElementById('todo-text');
	var textValue = input.value;

	if (textValue) {
    var listItem = document.createElement("li");
    listItem.innerHTML = textValue;

    var button = document.createElement("button");
    button.innerHTML = "X";
    button.addEventListener('click', removeToDo);
    listItem.appendChild(button);

    document.getElementById("todo-list").appendChild(listItem);
    input.value = "";
  }
}

function removeToDo(){
    var item = this.closest('li');
    item.remove();
  // var item = this.parentElement;
  // document.getElementById("todo-list").removeChild(item);
}
```
To render the UI client side:

```javascript
var app = document.getElementById('app');

function renderForm() {
  var form = document.createElement('form');
  form.classList.add('new-todo');

  var todoInput = document.createElement('input');
  todoInput.id = 'todo-text';
  var addButton = document.createElement('button');
  addButton.classList.add('add-button');
  addButton.innerText = '+';
  addButton.addEventListener('click', function(event) {
    event.preventDefault();
    addToDo(todoInput);
  });

  form.appendChild(todoInput);
  form.appendChild(addButton);

  app.appendChild(form);
}

function renderToDoList() {
  var listContainer = document.createElement('div');
  listContainer.classList.add('todo-list-container');
  var todoList = document.createElement('ul');
  todoList.id = 'todo-list';
  todoList.classList.add('todo-list');

  listContainer.appendChild(todoList);

  app.appendChild(listContainer);
}

function renderApp() {
  app.innerHTML = '';
  renderForm();
  renderToDoList();
}
renderApp();
```



## Step 2: Project Setup & Dependency Management

```
npm init
```

```
npm install --save-dev lite-server
```

```
npx lite-server 
```

Add a `start` script in `package.json`:

```json
"scripts": {
    "start": "lite-server"
  },
```
## Step 3: Adding Linting

```
npm install --save-dev eslint
```

```
npx eslint --init
```

Try one of the popular style guides ... however you will see that they don't fit the current project. You can run `npx eslint --init` again.
The best option right now is to go with "Answer questions about your style".

```
npx eslint src/**/*.js   
```

Fix the errors.

```
npx eslint src/**/*.js   --fix
```

Enable eslint integration in your IDE.

Create a script in `package.json`:
```json
"scripts": {
    "lint": "eslint src/**/*.js",
```
## Step 4: Adding jQuery Dependency

```
npm install jquery
```

Include jQuery from your `index.html`:

```
<script src="node_modules/jquery/dist/jquery.min.js"></script>
```

Rewrite your `app.js` to use jQuery.

The `app.js`  might then look like this:

```js
var $app = $('#app');

function renderForm() {
  var $input = $('<input>', {
    id: 'todo-text',
    placeholder: 'What do you want to do?'
  });
  $('<form>')
    .addClass('new-todo')
    .append(
      $input,
      $('<button>')
        .addClass('add-button')
        .text('+')
        .on('click', function(event) {
          event.preventDefault();
          addToDo($input);
        })
    )
    .appendTo($app);
}

function renderToDoList() {
  $('<div>')
    .addClass('todo-list-container')
    .append(
      $('<ul>')
        .prop('id', 'todo-list')
        .addClass('todo-list')
    )
    .appendTo($app);
}

function renderApp() {
  $app.html('');
  renderForm();
  renderToDoList();
}

renderApp();

function addToDo($input) {
  var textValue = $input.val();
  if (textValue) {
    $('<li>')
      .appendTo('#todo-list')
      .text(textValue)
      .append(
        $('<button>')
          .text('X')
          .on('click', removeToDo)
      );
    $input.val('');
  }
}

function removeToDo() {
  var item = $(this).closest('li');
  item.remove();
}
```


Fix the linting errors by adding to `.eslintrc.js`:

```js
"env": {
   "jquery": true,
 }
```


Now add the jQuery FitText plugin:

```
npm install fittextjs
```

Add the script:
```html
<script src="node_modules/fittextjs/index.js"></script>
```

Add some code in `layout.js`:
```js
$('#title').fitText(0.7);
```



## Step 5: Adding Modules & Bundling

Export a function from `layout.js`

```javascript
export function applyLayout() {
  $('#title').fitText(0.7);
}
```

Import and call the function in `app.js`:

```javascript
import { applyLayout } from './layout.js';
applyLayout();
```

Ensure that the `app.js` is loaded as a EcmaScript module:

```html
<script type="module" src="src/app/app.js"></script>
```

Run the app in a modern browser and observe the network requests.





```
npm install --save-dev webpack webpack-cli
```

Run webpack:
```
npx webpack src/app/app.js
```

Inspect the bundle `dist/main.js`

Rebuild the bundle with:
```
npx webpack src/app/app.js --mode=development --devtool=source-map      
```

Reference the bundle instead of `app.js`:
```html
<script src="dist/main.js"></script>
```

And remove the script tag for `layout.js` in `index.html`.

Add a build script to `package.json`:

```json
 "scripts": {
    "build": "webpack src/app/app.js --watch --mode=development --devtool=source-map"
  },
```



Import `jQuery` and `fittextjs` into `app.js` respectively `layout.js`:

```
import $ from 'jquery';
import 'fittextjs';
```

And remove the corresponding script tags.

Rebuild the bundle: `npm run build`
Inspect the bundle again.

Note: you can use `import * as $ from 'jquery'` or `import $ from 'jquery'`. WebPack assumes a default export in a module that does not provide a default export.


Create a file `webpack.config.js` in the root of your project:

```js
/* eslint-env node */

const path = require('path');

module.exports = {
    entry: './src/app/app.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'source-map'
};
```

Edit build scripts in `package.json`:
```json
 "scripts": {
    "build": "webpack --watch --mode=development",
    "build:prod": "webpack --mode=production"
  },
```


### Clean script

```
npm i -D rimraf
```

In `package.json`:

```json
 "scripts": {
    "clean": "rimraf dist",
}
```



## Step 6: Cache-Busting & CSS Bundling


### Cachebusting

Change your `webpack.config.js`

```json
	output: {
        filename: 'bundle.[contenthash].js'
    },
```

Inspect the generated bundle name in `dist`.

Now the app is broken, since `index.html` does not reference the correct bundle name ...

**Let's fix that by letting webpack generate the `index.html`:**

Move `index.html` into `src`. And remove the script tag.

```
npm i -D html-webpack-plugin
```

Change `webpack.config.js`:

```js
/* eslint-env node */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
   ...
    plugins: [
        new HtmlWebpackPlugin({template: 'src/index.html'})
    ]
};
```

Execute the build and inspect the the contents of `public`. Open `public/index.html`.

The app is still broken, since `index.html` does not reference the css files correctly.

**Let's fix that by letting webpack bundle the CSS`:`**

Remove the link to the stylesheet in `index.html`.

```
npm install --save-dev style-loader css-loader
```

Extend `webpack.config.json`:
```js
module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
```

Import css in `app.js`:
```
import '../css/index.css';
```

The css is now loaded as part of the js-bundle.

Let’s extract the css into a style-sheet:
```
npm install --save-dev mini-css-extract-plugin
```

Change `webpack.config.js`:
```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

  plugins: [
	new HtmlWebpackPlugin({ template: 'src/index.html' }),
	new MiniCssExtractPlugin({
	  filename: '[name].[contenthash].css',
	})
  ],
  module: {
		rules: [
	  	{	test: /\.css$/,	use: [ {loader: MiniCssExtractPlugin.loader}, 'css-loader'] }
		]
  }
```

Execute the build and inspect the the contents of `public`. Open `public/index.html`.

## Step 7: Code Splitting

Change `webpack.config.js`:

```js
optimization:{
    splitChunks: {
      chunks: 'all'
    }
	},
```

Webpack has a default optimization to split imports from `node_modules` into a `vendor` chunk.

**Note:** 

Code splitting can be configured in various ways. Here are some links:

- https://webpack.js.org/guides/code-splitting/
- https://webpack.js.org/plugins/split-chunks-plugin/
- https://medium.com/dailyjs/webpack-4-splitchunks-plugin-d9fbbe091fd0
- https://medium.com/hackernoon/the-100-correct-way-to-split-your-chunks-with-webpack-f8a9df5b7758



**Introduce dynamic import:**

Extract teh file `removeItem.js`:

```javascript
export function removeItem($button) {
  var item = $button.closest('li');
  item.remove();
}
```

And import it on demand:

```javascript
$('<button>')
  .text('X')
  .on('click', function() {
    var $button = $(this);
    import('./removeItem').then(function(m) {
      m.removeItem($button);
    });
  })
```





## Step 8: Webpack Dev Server

```
npm i -D webpack-dev-server
```

Start the dev server: `npx webpack-dev-server`

Change the build script in `package.json`:
```json
 "scripts": {
    "start": "webpack-dev-server --open --port 4567 --mode=development"
  }
```

Change something in the sources and observe live-reloading in the browser.

## Step 9: Using ES2015+ Features

Check that the current app is running in Internet Explorer.

Change `var` declarations to `let`/`const` declarations.

Simplify the removve functionality with an arrow function:

```js
 $('<button>')
   .text('X')
   .on('click', function() {
   import('./removeItem').then(m => {
     m.removeItem($(this));
   });
 })
```

Now the app is broken for Internet Explorer, since IE does not support arrow functions!

```
npm i -D @babel/core @babel/preset-env babel-loader
```

Add a `babel.config.js`:
```js
/* eslint-env node*/
module.exports = function(api) {
  const presets = [
    ['@babel/env', {
      targets: {
        ie: '11',
        edge: '17',
        firefox: '60',
        chrome: '67',
        safari: '11.1'
      },
      useBuiltIns: 'entry',
      corejs: 3,
      modules: false
    }]
  ];
  const plugins = [];

  api.cache(true);

  return { presets, plugins };
};
```

Add a loader to `webpack.config.js`:
```js
module: {
        rules: [
            {test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
            ...
        ]
    },
```

Now, adding todos works in IE11. But removing still fails ...

To emulate a full ES2015+ environment in old browsers you also need to add several polyfills:

```
npm install --save core-js
```
And import `core-js` in `app.js`:

```javascript
import 'core-js/stable'
```



## Step 10: Extracting a Model

Introduce a `model.ts`:

```javascript
class ToDo {
  title = '';
  constructor(title) {
    this.title = title;
  }
}

export class ToDoModel {
  newToDo: ToDo;
  toDoList: ToDo[];
  private $model: any;
  constructor() {
    this.newToDo = new ToDo('');
    this.toDoList = [new ToDo('Learn JavaScript'), new ToDo('Learn React')];
    this.$model = $(this);
  }

  addToDo() {
    this.toDoList.push(this.newToDo);
    this.newToDo = new ToDo('');
    this.notifyChanges();
  }

  removeToDo(toDo: ToDo) {
    toDo.title.toUpperCase();
    this.toDoList.splice(this.toDoList.indexOf(toDo), 1);
    this.notifyChanges();
  }

  notifyChanges() {
    this.$model.trigger('modelchange');
  }
}
```

Render the model on page load:

```javascript
const toDoModel = new ToDoModel();
renderApp(toDoModel);
```

Call `addTodo` and `removeToDo` on the model in the event handlers.



## Step 11: Adding Unit Tests

```
npm i -D jest 
```

Configure Babel to transpile ES2015 modules to commonjs modules for tests.
Change `babel.config.js`:

```js
module.exports = function(api) {

  const isTest = api.env('test');

  const presets = [['@babel/env', {
    targets: {
      ie: '11',
      edge: '17',
      firefox: '60',
      chrome: '67',
      safari: '11.1'
    },
    useBuiltIns: 'usage',
	corejs: 2,
    modules: isTest ? 'commonjs' : false
  }]];
  const plugins = [];

  api.cache(true);

  return {
    presets,
    plugins
  };
};

```

Write a first test in `model.test.js`:

```js
import model from './model';

test('model is created', () => {
  expect(model).toBeDefined();
});

test('add item', () => {
  model.addToDo();
  expect(model.toDoList).toHaveLength(3);
});

test('remove item', () => {
  model.removeToDo(model.toDoList[0]);
  expect(model.toDoList).toHaveLength(2);
});
```

Extend `package.json`:

```
scripts{
    "test": "jest",
    "test:watch": "jest --watch",
```

Fix the linting errors by adding to `.eslintrc.js`:

```js
"env": {
   "jest": true,
 }
```



## Step 12: Adding End-2-End Tests

```
npm i -D cypress
```

Extend `package.json`:
```
scripts{
    "e2e:dev": "cypress open",
    "e2e:run": "cypress run",
```

Open cypress: `npm run e2e:dev`.

Change `cypress.json`:
```
"testFiles": "todo/**/*.spec.js"
```

Write a spec for the ToDo application in `cypress/integration/todo/todo.spec.js`:
```javascript
context('Manage ToDo items', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4567/');
  });

  it('Add a todo item then remove it again', () => {
    cy.get('#todo-text')
      .type('Learn Cypress').should('have.value', 'Learn Cypress');

    cy.get('#add-button').click();

    let itemCountAfterAdd;
    cy.get('#todo-list li')
      .then(($items) => {
        itemCountAfterAdd = $items.length;
      })
      .last()
      .should('contain', 'Learn Cypress');

    cy.get('#todo-list li button')
      .last()
      .click();

    cy.get('#todo-list')
      .should('not.contain', 'Learn Cypress');

    cy.get('#todo-list li')
      .then(($items) => {
        expect($items.length).to.eq(itemCountAfterAdd - 1);
      });
  });
});

```

Fixing ESLint:
```
npm i -D eslint-plugin-cypress
```

Extend `.eslintrc.js`:
```
  extends: ['eslint:recommended', 'plugin:cypress/recommended'],
```

Fixing unit tests: add a `jest.config.js`

```
/* eslint-env node */
module.exports = {
  roots: ['src'],
};
```





## Step 13: Adding a Type System

Change the file `model.js` to `model.ts`. The IDE will probably show compilation errors.



**Using Babel to transpile TypeScript:**

Install:

```
npm i -D @babel/preset-typescript @babel/plugin-proposal-class-properties
```

Extend `babl.config.js`:

```javascript
const presets = [ ... , '@babel/preset-typescript'];
```

```javascript
const plugins = ['@babel/proposal-class-properties'];
```



Extend `webpack.config.js`:

```js
resolve: {
	extensions: ['.ts', '.js']
},
```

```javascript
module: {
        rules: [
            { test: /\.ts$/, use: 'babel-loader', exclude: /node_modules/ },   
```



**Using the TypeScript Compiler:**

```
npm i -D typescript
```

Add a file `tsconfig.json` to the root of the project:

```json
{
    "compilerOptions": {
    "module": "es2020",
    "target": "es5",
    "sourceMap": true,
    "lib": ["es2015", "dom"],
    "moduleResolution": "node",
    "esModuleInterop": true
  },
  "exclude": [
    "node_modules"
  ]
}
```

Extend `package.json`:

```json
scripts {
    "compile": "tsc --noEmit",
}
```



**Switching from Babel to TypeScript for transpilation**

```
npm i -D ts-loader
```

Change `webpack.config.js`:
```javascript
module: {
        rules: [
            {test: /\.ts$/, use: 'ts-loader'},
```





**Configure ESLint for TypeScript**

```
 npm i -D @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

Adjust `eslintrc.js`:

```js
/* eslint-env node */
module.exports = {
  env: {
    browser: true,
    es2021: true,
    jquery: true,
    jest: true
  },
  extends: [
    'eslint:recommended',
    'plugin:cypress/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  plugins: ['@typescript-eslint'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module'
  },
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always']
  }
};
```

Adjust `package.json`:

```
"scripts": {
	"lint": "eslint src/**/*.{ts,js}",
```



**Convert the rest of the source to Typescript**

Change all the filenames to `.ts`.

Add typings:

```
npm i -D @types/jest  
npm i -D @types/jquery
```

Extend the jQuery definition with fitText in a declaration file `types.d.ts`:

```javascript
/* eslint-disable @typescript-eslint/no-unused-vars */
interface JQuery {
  fitText(ratio: number): void;
}
```



**Configure ESLint for TypeScript**

```
 npm i -D @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

Adjust `eslintrc.js`:
```js
/* eslint-env node */
module.exports = {
  env: {
    browser: true,
    es2021: true,
    jquery: true,
    jest: true
  },
  extends: [
    'eslint:recommended',
    'plugin:cypress/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  plugins: ['@typescript-eslint'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module'
  },
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always']
  }
};
```

Add a script in `package.json` to perform the linting and configure your IDE to respect your linting rules.



**Jest and TypeScript**

Jest works out of the box with TypeScript when Babel is configured to transpile TypeScript. But then the test sources are not checked by the typescript compiler (but the IDE probably does that anyway).

A better alternative could be to use ts-jests:

```
npm i -D ts-jest
```

Modify `jest.config.js`

```javascript
/* eslint-env node */
module.exports = {
  roots: ['src'],
  preset: 'ts-jest'
};
```



## Step 14: Adding Backend Access

Start the API:

```
cd _todo-api
npm i
npm start
```

Load the data from the API when the model is created:
In `api.ts`:

```js
export function loadToDos() {
  return fetch(API_URL)
    .then(checkStatus)
    .then(resp => resp.json())
    .then(resp => resp.data);
}
```

In `model.ts`:

```js
return api.loadToDos()
        .then((toDoList) => {
            model.toDoList = toDoList;
            return model;
        });
```

In `app.ts`:

```js
modelPromise
    .then((data) => {
        model = data;
        $(model).on('modelchange', () => {
                renderNewToDo();
                renderToDos();
        });
        renderToDos();
    });
```

Also implement saving a ToDo to the server. 
Study [https://developer.mozilla.org/en-US/docs/Web/API/Fetch\_API/Using\_Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)

Implement a simple error handling.

Optional: Implement deleting a ToDo from the server.

**IE compatibility**
Check if the app works in IE.

Install a polyfill for fetch:
```
npm i -D whatwg-fetch
```

And import it:
```
import 'whatwg-fetch'
```

Now the app should work in IE again.