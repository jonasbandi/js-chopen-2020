/* eslint-env node*/

module.exports = function(api) {
  const isTest = api.env('test');
  const presets = [
    [
      '@babel/env',
      {
        targets: {
          // ie: '11',
          edge: '17',
          firefox: '60',
          chrome: '67',
          safari: '11.1'
        },
        useBuiltIns: 'entry',
        corejs: 3,
        modules: isTest ? 'commonjs' : false
      }
    ],
    '@babel/preset-typescript'
  ];
  const plugins = ['@babel/proposal-class-properties'];

  api.cache(true);

  return { presets, plugins };
};
