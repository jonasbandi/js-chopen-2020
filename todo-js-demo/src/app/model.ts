import $ from 'jquery';

class ToDo {
  title = '';
  constructor(title) {
    this.title = title;
  }
}

export class ToDoModel {
  newToDo: ToDo;
  toDoList: ToDo[];
  private $model: any;
  constructor() {
    this.newToDo = new ToDo('');
    this.toDoList = [new ToDo('Learn JavaScript'), new ToDo('Learn React')];
    this.$model = $(this);
  }

  addToDo() {
    this.toDoList.push(this.newToDo);
    this.newToDo = new ToDo('');
    this.notifyChanges();
  }

  removeToDo(toDo: ToDo) {
    toDo.title.toUpperCase();
    this.toDoList.splice(this.toDoList.indexOf(toDo), 1);
    this.notifyChanges();
  }

  notifyChanges() {
    this.$model.trigger('modelchange');
  }
}
