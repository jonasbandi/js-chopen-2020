import '../css/index.css';
import $ from 'jquery';
import { renderApp } from './uiRenderer';
import { renderLayout } from './layout';
import { ToDoModel } from './model';

console.log('ToDo: Implement the functionality ... ');

let toDoModel = new ToDoModel();

$(toDoModel).on('modelchange', () => {
  console.log('Model changed', toDoModel);
  renderApp(toDoModel);
});

renderLayout();
renderApp(toDoModel);

class Person {
  greet() {
    console.log('Hello World!');
  }
}

let person = new Person();
person.greet();
