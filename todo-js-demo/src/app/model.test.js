import { ToDoModel } from './model';

let model;
beforeEach(() => {
  model = new ToDoModel();
});

test('model is created', () => {
  expect(model).toBeDefined();
});

test('add item', () => {
  model.addToDo();
  expect(model.toDoList).toHaveLength(3);
});

test('remove item', () => {
  model.removeToDo(model.toDoList[0]);
  expect(model.toDoList).toHaveLength(1);
});
