import $ from 'jquery';
import { addToDo } from './todoLogic';
import { ToDoModel } from './model';

console.log('UI renderer loaded');

var $app = $('#app');

function renderForm(toDoModel: ToDoModel) {
  var $input = $('<input>', {
    id: 'todo-text',
    placeholder: 'What do you want to do?'
  }).on('blur', () => {
    toDoModel.newToDo.title = $input.val();
  });
  $('<form>')
    .addClass('new-todo')
    .append(
      $input,
      $('<button>', { id: 'add-button' })
        .addClass('add-button')
        .text('+')
        .on('click', function(event) {
          event.preventDefault();
          // addToDo($input);
          toDoModel.addToDo();
        })
    )
    .appendTo($app);
}

function renderToDoList(toDoModel: ToDoModel) {
  const $todoList = $('<ul>')
    .prop('id', 'todo-list')
    .addClass('todo-list');
  $('<div>')
    .addClass('todo-list-container')
    .append($todoList)
    .appendTo($app);

  for (const toDo of toDoModel.toDoList) {
    $('<li>')
      .appendTo($todoList)
      .text(toDo.title)
      .append(
        $('<button>')
          .text('X')
          .on('click', () => {
            toDoModel.$model = '';
            toDoModel.removeToDo(toDo);
          })
      );
  }
}

export function renderApp(toDoModel) {
  $app.html('');
  renderForm(toDoModel);
  renderToDoList(toDoModel);
}
