import $ from 'jquery';

export function addToDo($input) {
  const textValue = $input.val();
  if (textValue) {
    $('<li>')
      .appendTo('#todo-list')
      .text(textValue)
      .append(
        $('<button>')
          .text('X')
          .on('click', removeToDo)
      );
    $input.val('');
  }
}

function removeToDo() {
  import('./funny.js').then(m => {
    console.log('Funny loaded!');
    m.saySomething();
  });

  // const item = $(this).closest('li');
  // item.remove();
}

console.log('UI Logic Loaded');
