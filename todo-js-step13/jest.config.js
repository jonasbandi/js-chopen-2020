/* eslint-env node */
module.exports = {
  roots: ['src'],
  preset: 'ts-jest',
  reporters: ['default', ['jest-junit', { outputDirectory: './build-info' }]]
};
